import Actions from '../monstre-1/Actions.js';
import ui from '../monstre-2/ui.js';

let b1, b2, b3, b4, b5, b6, b7, bKILL;


function start() {
    Actions.init("Le monstre", 15, 20, true);
    let m = Actions.get();
    b6 = document.querySelector("#b6");

    ui.log("vie: " + m.vie + " argent: " + m.argent);

    ui.displayStatus();
    b6.addEventListener("click", () => {
        let m = Actions.get();
        alert(m.nom + " " + m.vie + " " + m.argent);
    });

    b2 = document.querySelector("#b2");
    b2.addEventListener("click", () => {
        Actions.run();
    });

    b3 = document.querySelector("#b3");
    b3.addEventListener("click", () => {
        Actions.fight();
    });

    b7 = document.querySelector("#b7");
    b7.addEventListener("click", () => {
        Actions.work();
    });

    b5 = document.querySelector("#b5");
    b5.addEventListener("click", () => {
        Actions.eat();
    });

    b4 = document.querySelector("#b4");
    b4.addEventListener("click", () => {
        Actions.sleep();
    })


    b1 = document.querySelector("#b1");
    b1.addEventListener("click", () => {
        Actions.newlife();
    });

    bKILL = document.querySelector("#k");
    bKILL.addEventListener("click", () => {
        Actions.kill();
    });


    setInterval(() => {
        if (Actions.get().vie > 0 && Actions.get().etat) {
            Actions.pertePv();
            ui.displayStatus();
            let nb = Math.floor(Math.random() * Math.floor(4));
            switch (nb) {
                case 0:
                    Actions.run();
                    break;
                case 1:
                    Actions.fight();
                    break;
                case 2:
                    Actions.work();
                    break;
                case 3:
                    Actions.eat();
                    break;
            }
        }
    }, 12000);


}

export default {
    start: start,
}