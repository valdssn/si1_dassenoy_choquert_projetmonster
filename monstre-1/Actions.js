import ui from "../monstre-2/ui.js";

let name;
let life;
let money;
let awake;

function get(){
    return {
        nom:name,
        vie:life,
        argent:money,
        etat:awake
    }
}

 function init(n, l, m, a){
    name = n;
    life = l;
    money = m;
    awake = a;
}

function run(){
    if(life>0 && awake) {
        life = life - 1;
        ui.displayStatus();
        ui.log(name + " court et perd 1 pv");
    }
}

function fight(){
    if(life>2 && awake) {
        life = life -3;
        ui.displayStatus();
        ui.log(name + " combat et perd 3 pv");
    }
}

function work(){
    if(life >0 && awake) {
        life = life -1;
        money = money+2;
        ui.displayStatus();
        ui.log(name+ " travaille et perd 1 pv et gagne 2 euros");
    }
}

function eat(){
    if(life>0 && money >2 && awake) {
        life = life+2;
        money = money -3;
        ui.displayStatus();
        ui.log(name + " mange et gagne 2 pv et perd 3 euros");
    }
}

function sleep(){
    if (awake && life>0){
        awake= false;
        ui.log(name + " s'endort");
        ui.displayStatus();
        setTimeout(()=>{
            awake = true;
            life = life+1;
            ui.log(name + " se réveille et gagne 1 pv");
            ui.displayStatus();
        },10000);
    }
}

function pertePv(){
    if(awake){ life --;}
}

function kill() {
    if (life>0){
        life = 0;
        ui.log(name+" est mort");
        ui.displayStatus();
    }
}

function newlife() {
    if (life<=0){
        ui.log(name+" respawn");
        awake = true;
        life = 15;
        money = 20;
        ui.displayStatus();
    }
}

export default{
    init:init,
    get: get,
    run:run,
    fight:fight,
    work:work,
    eat:eat,
    sleep:sleep,
    pertePv:pertePv,
    kill:kill,
    newlife:newlife,
}