import Actions from '../monstre-1/Actions.js';

function log(message){
    let d = document.getElementById("actionbox");
    let p = document.createElement("P");
    let t = document.createTextNode(message);
    p.appendChild(t);
    let first = d.firstChild;
    d.insertBefore(p,first);

}

function displayStatus(){
    let m = Actions.get();
    let liste = document.getElementById("status");
    let puces = liste.querySelectorAll("li");
    let boxMonster = document.getElementById("monster");

    puces[0].innerHTML = "life: "+m.vie;
    puces[1].innerHTML = "money: "+m.argent;
    if(m.etat === true){
        puces[2].innerHTML = "awake";
    }else{
        puces[2].innerHTML = "asleep";
    }

    while (boxMonster.classList.length > 0) {
        boxMonster.classList.remove(boxMonster.classList.item(0));
    }
    boxMonster.classList.add("monster");

    switch (true) {
        case (m.vie < 5):
            boxMonster.classList.toggle("rouge");
            break;
        case (m.vie < 10):
            boxMonster.classList.toggle("orange");
            break;
        case (m.vie < 15):
            boxMonster.classList.toggle("bleu");
            break;
        default:
            boxMonster.classList.toggle("vert");
            break
    }

    switch (true) {
        case (m.argent < 20):
            boxMonster.classList.toggle("pauvre");
            break;
        case (m.argent < 40):
            boxMonster.classList.toggle("moyen");
            break;
        case (m.argent < 60):
            boxMonster.classList.toggle("riche");
            break;
        default:
            boxMonster.classList.toggle("superRiche");
            break;
    }
}

export default{
    log:log,
    displayStatus: displayStatus
}
